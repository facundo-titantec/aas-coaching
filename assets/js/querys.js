// ACTIVACION DE TOOLTIPS, POPOVERS & CAROUSEL DE BOOTSTRAP
$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
      interval: 100
    })
  }
);

// MODIFICANDO EL MODAL
$("#newsletter").on('show.bs.modal',function(response){
  console.log("mostrando");
  $("#newsletterBtn").removeClass("btn-outline-light");
  $("#newsletterBtn").addClass("btn-outline-danger");
  $("#newsletterBtn").prop('disabled',true);
});
$("#newsletter").on('shown.bs.modal',function(response){
  console.log("se mostro");
});
$("#newsletter").on('hide.bs.modal',function(response){
  console.log("oculto");
  $("#newsletterBtn").removeClass("btn-outline-danger");
  $("#newsletterBtn").addClass("btn-outline-light");
  $("#newsletterBtn").prop('disabled',false);
});
$("#newsletter").on('hidden.bs.modal',function(response){
  console.log("se oculto");
});
//endbuild
module.exports = function (grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        sass:{
            dist:{
                files:[{
                    expand: true,
                    cwd: 'assets/css',
                    src: ['*.scss'],
                    dest: 'assets/css',
                    ext: '.css'
                }]
            }
        },
        watch:{
            files:['assets/css/*.scss'],
            tasks: ['css'] 
        },
        browserSync: {
            bsFiles: {
                src : [
                    'assets/css/*.css',
                    'assets/js/*.js',
                    '*.html'
            ]},
            options: {
                server: {
                    server:{
                        baseDir: "./", // directorio base de nuestro servidor
                    },
                    watchTask: true
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    //src: ['**/*.{png,jpg,gif}'],
                    src: ['assets/img/*.{png,jpg,gif,svg}'],
                    dest: 'dist'
                }]
            }
        },
        copy: {
            html:{
                files:[{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dist: 'dist/'
                }]
            },
        },
        clean:{
            build:{
                src:['dist/']
            }
        },
        cssmin:{
            dist: {
                files:[{
                    expand: true,
                    cwd:'./',
                    src: 'assets/css/*.css',
                    dest:'dist/'
                }]
            }
        },
        uglify:{
            dist: {
                files:[{
                    expand: true,
                    cwd:'./',
                    src: 'assets/js/*.js',
                    dest:'dist/'
                }]
            }
        },
        filerev:{
            options:{
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release:{
                files:[{
                    src:[
                        'dist/assets/js/*.js',
                        'dist/assets/css/*.css'
                    ]
                }]
            }
        },
        concat:{
            options:{
                separator: ';'
            },
            dist: {
                files:[{
                    expand: true,
                    cwd:'./',
                    src: '*.html',
                    dest:'dist/'
                }]
            }
        },
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: [
                    'index.html',
                    'aboutus.html',
                    'contact.html',
                    'payment.html',
                    'lectures.html',
                    'session-consult.html',
                    'session-reserve.html',
                    'terms.html'
                    ]
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function(context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false
                                }
                            }
                        }]
                    }
                }
            }
        },
        usemin: {
            html: [
                'dist/index.html',
                'dist/aboutus.html',
                'dist/contact.html',
                'dist/payment.html',
                'dist/lectures.html',
                'dist/session-consult.html',
                'dist/session-reserve.html',
                'dist/terms.html'
                ],
            options: {
                assetsDir: ['dist', 'dist/assets/css', 'dist/assets/js']
            }
        }
    });
    // grunt.loadNpmTasks('grunt-contrib-watch');    // NO NECESARIO XQ SE CARGA JIT-GRUNT
    // grunt.loadNpmTasks('grunt-contrib-sass');     // NO NECESARIO XQ SE CARGA JIT-GRUNT
    // grunt.loadNpmTasks('grunt-browser-sync');     // NO NECESARIO XQ SE CARGA JIT-GRUNT
    // grunt.loadNpmTasks('grunt-contrib-imagemin'); // NO NECESARIO XQ SE CARGA JIT-GRUNT
    grunt.registerTask('css', ['sass']);;
    grunt.registerTask('img:compress', ['imagemin']);;
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('build', [
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
        ]);
        };